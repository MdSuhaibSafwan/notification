from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator
from channels.auth import AuthMiddlewareStack
from django.core.asgi import get_asgi_application
from django.urls import path
from messaging.consumers import NotificationConsumer, MessageConsumer
from django.urls import re_path
from django.conf.urls import url

websocket_urlpatterns = [
    path(
        "notification/",
        NotificationConsumer.as_asgi(),
    ),

    path(
        "message/<token>/", 
        MessageConsumer.as_asgi(), 
    ),
    
]

application = ProtocolTypeRouter(
    {
        "websocket": AuthMiddlewareStack(URLRouter(websocket_urlpatterns)),
    }
)
