from django.conf.urls import include
from django.contrib import admin
from django.urls import path
from messaging.views import index, message_user

urlpatterns = [
    path('admin/', admin.site.urls),
    path("", index),
    path("message/<username>/", message_user),
    path("api/", include("messaging.api.urls")),
    path("api/", include("notification.api.urls")),

    path("rest-auth/", include("rest_framework.urls")),
]
