from django.contrib import admin
from .models import ChatingRoom, ChatingRoomMessage, ChatGroup

# admin.site.register(Notification)
# admin.site.register(Post)
# admin.site.register(Comment)
# admin.site.register(Reply)
admin.site.register(ChatGroup)
admin.site.register(ChatingRoom)
admin.site.register(ChatingRoomMessage)
