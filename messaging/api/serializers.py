from rest_framework import serializers
from messaging.models import ChatingRoomMessage, ChatingRoom, ChatGroup, UserActiveHistory
from django.contrib.auth import get_user_model
from notification.api.serializers import UserSerializer

User = get_user_model()


class ChatingRoomMessageListSerializer(serializers.ModelSerializer):
    sent_by_user = serializers.StringRelatedField(read_only=True)
    to_user = serializers.SerializerMethodField()

    class Meta:
        model = ChatingRoomMessage
        exclude = ["chat_room", "user1", "user2"]

    def get_to_user(self, serializer):
        user_1 = serializer.user1
        user_2 = serializer.user2
        sent_user = serializer.sent_by_user
        if serializer.group:
            return None

        if sent_user == user_1:
            return user_2.username

        return user_1.username


class MessageCreateSerializer(serializers.ModelSerializer):
    user1 = serializers.StringRelatedField(read_only=True)
    user2 = serializers.StringRelatedField(read_only=True)
    sent_by_user = serializers.StringRelatedField(read_only=True)
    chat_room = serializers.StringRelatedField(read_only=True)
    slug = serializers.StringRelatedField(read_only=True)
    seen = serializers.BooleanField(read_only=True)
    group = serializers.StringRelatedField(read_only=True)
    message = serializers.CharField(required=False)

    class Meta:
        model = ChatingRoomMessage
        fields = "__all__"

    def validate(self, attrs):
        message = attrs.get("message")
        image = attrs.get("image")
        if ((not message) or (message=="")) and (not image):
            raise serializers.ValidationError("no")

        return super().validate(attrs)


class ChatGroupListCreateSerializer(serializers.ModelSerializer):
    slug = serializers.SlugField(read_only=True)

    class Meta:
        model = ChatGroup
        exclude = ["users", ]


class ChatGroupDetailSerializer(serializers.ModelSerializer):
    slug = serializers.SlugField(read_only=True)
    users = serializers.SerializerMethodField()

    class Meta:
        model = ChatGroup
        fields = "__all__"

    def get_users(self, serializer):
        qs = serializer.users.all()
        serialized = UserSerializer(qs, many=True)
        return serialized.data


class UserActiveHistorySerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = UserActiveHistory
        fields = "__all__"

