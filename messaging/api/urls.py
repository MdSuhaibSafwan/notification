from .views import (MessageCreateAPIVIew, RoomMessagesListAPIView, GroupMessageCreateApiView, 
                create_websocket_token, UserActiveHistoryAPIView, ChatGroupListCreateAPIView, ChatGroupRetrieveAPIView, add_users_to_group)
from django.urls import path

urlpatterns = [
    path("room/<username>/", RoomMessagesListAPIView.as_view(), ),
    path("message/<username>/", MessageCreateAPIVIew.as_view(), ),
    path("message/group/<group_slug>/", GroupMessageCreateApiView.as_view(), ),
    path("user/websocket_token/create/", create_websocket_token),
    path("groups/list_create/", ChatGroupListCreateAPIView.as_view(), ),  # creating group
    path("group/detail/<slug>/", ChatGroupRetrieveAPIView.as_view(), ),  # add user to group
    path("group/<slug>/add_users/", add_users_to_group, ),
    path("users/active/", UserActiveHistoryAPIView.as_view())
]

# Adding to see if user is active

