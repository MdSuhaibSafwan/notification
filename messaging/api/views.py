from django.contrib.auth import get_user_model
from ..models import ChatingRoomMessage, ChatingRoom,WebSocketToken, ChatGroup, ChatGroupUser, UserActiveHistory
from .serializers import ChatingRoomMessageListSerializer, MessageCreateSerializer, UserActiveHistorySerializer, ChatGroupDetailSerializer, ChatGroupListCreateSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.exceptions import NotFound, PermissionDenied, ValidationError
from django.shortcuts import get_object_or_404
from rest_framework.generics import CreateAPIView
from notification.api.helpers import get_object_or_rest_404
from rest_framework.response import Response
from rest_framework.decorators import permission_classes, api_view
from rest_framework import status


User = get_user_model()


class MessageCreateAPIVIew(CreateAPIView):
    serializer_class = MessageCreateSerializer
    lookup_url_kwarg = "username"

    def get_user(self):
        user = self.kwargs.get(self.lookup_url_kwarg)
        if user == self.request.user.username:
            raise ValidationError("Both user cannot be the same")
        return get_object_or_rest_404(User, username=user, msg="user not found")

    def perform_create(self, serializer):
        user2 = self.get_user()
        curr_user = self.request.user
        room = ChatingRoom.objects.get_or_create_room(curr_user.username, user2.username)
        print("Creating a message in Api view")
        serializer.save(user1=curr_user, user2=user2, sent_by_user=curr_user, chat_room=room)


class RoomMessagesListAPIView(ListAPIView):
    permission_classes = [IsAuthenticated, ]
    serializer_class = ChatingRoomMessageListSerializer
    lookup_url_kwarg = "username"

    def get_user_by_username(self):
        username = self.kwargs.get(self.lookup_url_kwarg)
        user = get_object_or_rest_404(User, username=username, msg="User with this Username is not Found...")
        if self.request.user == user:
            raise PermissionDenied("Room with own-self is defined...")
        print("got user from username -> ", user)
        return user

    def get_queryset(self):
        get_user = self.get_user_by_username()
        obj = ChatingRoom.objects.filter_room(get_user.username, self.request.user.username)
        if obj is None:
            return []

        print("Found a room for both users")
        qs = obj.ch_messages.all()
        print("Message Queryset")
        return qs


class GroupMessageCreateApiView(CreateAPIView):
    serializer_class = MessageCreateSerializer
    lookup_url_kwarg = "group_slug"

    def perform_create(self, serializer):
        group = self.get_group()
        print("Sending message to group")
        serializer.save(group=group, sent_by_user=self.request.user)

    def get_group(self):
        slug = self.kwargs.get(self.lookup_url_kwarg)
        print("Finding group by slug")
        return get_object_or_rest_404(ChatGroup, slug=slug, msg="group with this slug is not Found")


@api_view(["POST", ])
@permission_classes([IsAuthenticated, ])
def create_websocket_token(request):
    curr_user = request.user
    qs = curr_user.ws_token.filter(expired=False)
    for i in qs:
        i.expired = True
        i.save()

    print("Made all the token expired")

    data = {
        "status": "successfull",
        "token": str(curr_user.ws_token.create().token)
    }

    print("Created a new token and sending to api as a Response")

    return Response(data, status.HTTP_201_CREATED)


class ChatGroupListCreateAPIView(ListCreateAPIView):
    serializer_class = ChatGroupListCreateSerializer

    def get_queryset(self):
        return ChatGroup.objects.all()

    def perform_create(self, serializer):
        obj = serializer.save()
        print("Creating a group")
        ChatGroupUser.objects.create(chat_group=obj, user=self.request.user, role="admin")


class ChatGroupRetrieveAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = ChatGroupDetailSerializer
    lookup_url_kwarg = "slug"

    def get_object(self):
        slug = self.kwargs.get(self.lookup_url_kwarg)
        obj = get_object_or_rest_404(ChatGroup, slug=slug)
        return obj


@api_view(["POST"])
@permission_classes([IsAuthenticated, ])
def add_users_to_group(request, *args, **kwargs):
    slug = kwargs.get("slug")
    chat_group = get_object_or_rest_404(ChatGroup, slug=slug)
    data = request.data
    users_add = data.get("users")
    print(users_add, type(users_add))

    for i in users_add:
        user = User.objects.filter(username=i)
        if not user.exists():
            raise NotFound("User with this username does not exist")
        user = user.get()
        chat_group.users.add(user)

    print("Added users to Group")
        
    chat_group.save()

    data = {
        "status": "successfull"
    }
    return Response(data, status.HTTP_201_CREATED)


class UserActiveHistoryAPIView(ListAPIView):
    serializer_class = UserActiveHistorySerializer

    def get_queryset(self):
        qs = UserActiveHistory.objects.all()
        return qs

