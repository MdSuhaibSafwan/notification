import random
import json
from django.db.models.signals import post_save
from django.dispatch import receiver
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from .models import ChatingRoomMessage
from .api.serializers import ChatingRoomMessageListSerializer


def send_to_messaging_ws(instance_data, channel_layer, chat_room):
    async_to_sync(channel_layer.group_send)(
        chat_room,
        {
            "type": "send.message",
            "data": instance_data
        }
    )


@receiver(signal=post_save, sender=ChatingRoomMessage)
def websocket_send_message(sender, instance, created, **kwargs):
    if created:
        channel_layer = get_channel_layer()
        group = instance.group
        if group == None:
            username = instance.get_to_user
            chat_room = f"message_{username}"
            instance_data = json.dumps(ChatingRoomMessageListSerializer(instance).data)
            print("\n")
            print("From Signal", chat_room)
            send_to_messaging_ws(instance_data, channel_layer, chat_room)
            
        else:
            print("inside group") 
            instance_data = json.dumps(ChatingRoomMessageListSerializer(instance).data)
            users = group.users.all().exclude(id=instance.sent_by_user.id)
            for i in users:
                chat_room = f"message_{i.username}"
                print(chat_room)
                send_to_messaging_ws(instance_data, channel_layer, chat_room)
            print("\n")



