from django.http.response import Http404
from django.shortcuts import get_object_or_404, render
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required

User = get_user_model()


@login_required
def index(request):
    qs = User.objects.all()

    context = {
        "users": qs,
    }

    return render(request, "messaging/main.html", context)


@login_required
def message_user(request, username):
    curr_user = request.user
    obj = get_object_or_404(User, username=username)
    if curr_user == obj:
        raise Http404
    

    return render(request, "messaging/message.html", {"to_user": obj.username})
