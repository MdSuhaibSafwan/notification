from rest_framework.exceptions import NotFound
from rest_framework import pagination
from rest_framework.response import Response

def get_object_or_rest_404(klass, msg="NotFound", **kwargs):
    qs = klass.objects.filter(**kwargs)
    if qs.exists():
        return qs.get()

    raise NotFound(msg)


class CustomPagination(pagination.PageNumberPagination):
    page_size = 2
    page_size_query_param = 'page_size'

    def get_paginated_response(self, data):
        qs_notifications = self.request.all_notification
        # print(qs_notifications)
        total_unseen = qs_notifications.filter(seen=False).count()

        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'unseen': total_unseen,
            'results': data
        })
