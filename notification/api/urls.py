from .views import NotificationListAPIView, NotificationDetailtAPIView
from django.urls import path

urlpatterns = [
    path("notifications/", NotificationListAPIView.as_view()),
    path("notification/<id>/", NotificationDetailtAPIView.as_view(), ),
]
