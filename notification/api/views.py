from django.contrib.auth import get_user_model
from ..models import Notification
from .serializers import NotificationSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import ListAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.exceptions import NotFound, PermissionDenied, ValidationError
from django.shortcuts import get_object_or_404
from rest_framework.generics import CreateAPIView
from .helpers import get_object_or_rest_404, CustomPagination
from rest_framework.response import Response
from rest_framework.decorators import permission_classes, api_view
from rest_framework import status


User = get_user_model()

class NotificationListAPIView(ListAPIView):
    serializer_class = NotificationSerializer
    pagination_class = CustomPagination
    permission_classes = [IsAuthenticated, ]

    def get_all_notifications(self):
        curr_user = self.request.user
        qs = curr_user.notifications.all()
        return qs

    def get_queryset(self):
        qs = self.get_all_notifications()
        query = self.request.query_params.get("query", None)
        self.request.all_notification = qs
        if query:
            query = str(query).lower()
            if query == "seen":
                qs = qs.filter(seen=True)
            elif query == "unseen":
                qs = qs.filter(seen=False)
            else:
                raise PermissionDenied
        return qs


class NotificationDetailtAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = NotificationSerializer
    lookup_url_kwarg = "id"

    def get_object(self):
        return self.get_notification()

    def get_notification(self):
        not_id = self.kwargs.get(self.lookup_url_kwarg)
        obj = Notification.objects.get(id=not_id)
        if obj.user != self.request.user:
            raise PermissionDenied
        obj.seen = True; obj.save()
        return obj

