from django.db import models
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.utils.text import slugify
from random import choice
from string import ascii_letters
from django.utils import timezone

User = get_user_model()

def random_slug_gen(number=9):
    return "".join(choice(ascii_letters) for i in range(number))


# Create your models here.


class Notification(models.Model):
    CATEGORY = (
        ("LK", "LIKE"),
        ("CM", "COMMENT"),
        ("CR", "REPLY"),
    )

    from_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="from_user")
    to_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="notifications")
    notification_type = models.CharField(max_length=2, choices=CATEGORY, null=True)
    post = models.ForeignKey("Post", on_delete=models.SET_NULL, null=True)
    comment = models.ForeignKey("Comment", on_delete=models.SET_NULL, null=True)
    content = models.CharField(max_length=2000)
    seen = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    @property
    def get_type(self):
        post = self.post
        cmnt = self.comment
        category = dict(self.CATEGORY)[self.notification_type]
        if category == "LIKE" or category == "COMMENT":
            return post

        return cmnt




class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="posts")
    content = models.TextField()


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="comments")
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="post_comments")
    content = models.TextField()


class Reply(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="replies")
    comment = models.ForeignKey(Comment, on_delete=models.SET_NULL, null=True, related_name="cmnt_reply")
    content = models.TextField()
    reply = models.ForeignKey("self", on_delete=models.SET_NULL, null=True, blank=True, related_name="replies")

