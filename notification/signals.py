from .models import Notification, Post, Comment, Reply
import random
import json
from django.db.models.signals import post_save
from django.dispatch import receiver
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

@receiver(signal=post_save, sender=Notification)
def websocket_send_notification(sender, instance, created, **kwargs):
    # if created:
    channel_layer = get_channel_layer()
    chat_room = f"notification_room_{instance.to_user.username}"
    print("\n")
    print("From Signal", chat_room)

    async_to_sync(channel_layer.group_send)(
        chat_room,
        {
            "type": "send.notification",  # send_notification
            "data": instance.content
        }
    )

    print("\n")


@receiver(signal=post_save, sender=Post)
def create_notification_for_post(sender, instance, created, **kwargs):
    if created:
        from_user = instance.user
        to_user = instance.user
        content = ""
        Notification.objects.create(from_user=from_user, to_user=to_user, content=content)


@receiver(signal=post_save, sender=Comment)
def create_notification_for_comment(sender, instance, created, **kwargs):
    if created:
        cmnt_user = instance.user
        post_user = instance.post.user 
        msg = f"{cmnt_user} Commented on your post"
        # to_user = ["a", "b"]
        qs = Notification.objects.filter(
            from_user=cmnt_user, to_user=post_user, post=instance.post, 
            notification_type="CM",
        )
        if qs.exists():
            obj = qs.get()
            post_obj = obj.get_type
            qs = post_obj.post_comments.all()
            char = ""

            if qs.count() > 1:
                # users = []
                user = random.choice(qs).user
                char = f"{user} and {qs.count()-1} others has commented your post"
            #     counter = 0
            #     for i in range(3):
            #         user = random.choice(qs).user
            #         if user in users:
            #             char = f"{user} and {qs.count()-1} others has commented your post"
            #             break
            #         users.append(user)
            #         if i==2 and qs.count()-2>= 1:
            #             char = ", ".join(users[b] for b in users)
            #             char += f" and {qs.count()-2} others has commented on your post"
            #         else:
            #             char = "and ".join(b.username for b in users)
            #             char += " has commented on your post"
            else:
                char = qs.get().user.username + " has commented on your post"

            obj.content = char
            obj.save()
            return obj


        obj = Notification.objects.create(
            from_user=cmnt_user, to_user=post_user, content=msg, post=instance.post, 
            notification_type="CM",
        )
        return obj


@receiver(signal=post_save, sender=Reply)
def create_notification_for_reply(sender, instance, created, **kwargs):
    if created:
        reply_user = instance.user
        cmnt_user = instance.comment.user 
        msg_to_commenter = f"{reply_user} gave a reply to your comment"

        qs = Notification.objects.filter(
            from_user=reply_user, to_user=cmnt_user,
            comment=instance.comment, notification_type="CR"
        )
        if qs.exists():
            notification_obj = qs.get()
            cmnt_obj = notification_obj.get_type
            qs = cmnt_obj.cmnt_reply.all()
            if qs.count() > 1:
                msg_to_commenter = f"{reply_user} and {qs.count()-1} others gave a reply to your comment"

            notification_obj.content = msg_to_commenter
            notification_obj.save()
            return notification_obj

        post_user = instance.comment.post.user
        msg_to_poster = f"{reply_user} gave a reply on a comment in your post"

        Notification.objects.create(
            from_user=reply_user, to_user=cmnt_user, content=msg_to_commenter,
            comment=instance.comment, notification_type="CR"
        )

        # Notification.objects.create(from_user=reply_user, to_user=post_user, content=msg_to_poster)
        # recurring_notification(instance)


def recurring_notification(reply_obj):
    reply = reply_obj.reply
    msg = "a reply"
    if reply:
        recurring_notification(reply)
    Notification.objects.create(from_user=reply_obj.user, to_user=reply.user, content=msg)

